import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import BMICalPage from './pages/BMICalPage';
import Header from './components/Header';

import { Routes, Route } from "react-router-dom";
import LuckyNumberPage from './pages/LuckyNumberPage';

function App() {
  return (
    <div className="App">
        <Header />
        <Routes>
            <Route path="about" element={
                    <AboutUsPage />
                  } />
            
            <Route path="/" element={
                    <BMICalPage />
                  } />

            <Route path="lucky" element={
                      <LuckyNumberPage />
                  } />
        </Routes>
    </div>
  );
}

export default App;
